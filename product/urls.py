from django.urls import path

from product.views import MainPageView, ProductListView, ProductDetailView

app_name = 'product'

urlpatterns = (
    path('', ProductListView.as_view(), name='products-list'),
    path('all/', ProductListView.as_view(), name='products-list'),
    path('product/<int:pk>/about', ProductDetailView.as_view(), name='about'),
    path('category/<int:pk>/', MainPageView.as_view(), name='category'),
)
