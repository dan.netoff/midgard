from django.views.generic import ListView, DetailView
from product.models import Product, Category


class MainPageView(ListView):
    template_name = 'shop.html'
    model = Product
    context_object_name = 'products'

    def get_queryset(self):
        pk = self.kwargs['pk']
        if pk is not None:
            return self.model.objects.filter(category__id=pk)
        return super(MainPageView, self).get_queryset()

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['categories'] = Category.objects.all()
        return context


class ProductListView(ListView):
    template_name = 'shop.html'
    model = Product
    context_object_name = 'products'
    paginate_by = 12

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['categories'] = Category.objects.all()
        return context

    def get_queryset(self):
        return super(ProductListView, self).get_queryset()


class ProductDetailView(DetailView):
    template_name = 'product-single.html'
    model = Product
    context_object_name = 'product'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['categories'] = Category.objects.all()
        return context
