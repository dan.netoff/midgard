from django.db import models

from django.urls import reverse


def product_upload_root(instance, filename):
    return f'product/{instance.name}/{filename}'


def additional_images_upload_root(instance, filename):
    return f'product/{instance.product.name}/additional_images/{filename}'


class Product(models.Model):
    name = models.CharField('Название продукта', max_length=100)
    full_description = models.TextField('Полное описание', blank=True, default='Нет описания')
    short_description = models.CharField('Краткое описание', max_length=100, blank=True, default='Нет описания')
    main_photo = models.ImageField('Главная картинка', upload_to=product_upload_root)
    proteins = models.DecimalField('Белки', max_digits=5, decimal_places=2)
    fats = models.DecimalField('Жиры', max_digits=5, decimal_places=2)
    carbohydrates = models.DecimalField('Углеводы', max_digits=5, decimal_places=2)
    price = models.DecimalField('Цена за штуку', max_digits=11, decimal_places=2)
    popularity = models.BigIntegerField('Популярность', default=0,)
    category = models.ForeignKey('Category', related_name='products', verbose_name='Категории',
                                 on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('product:about', args=[self.id])


class Category(models.Model):
    name = models.CharField('Название категории', max_length=20, unique=True)

    class Meta:
        verbose_name_plural = 'Categories'

    def __str__(self):
        return self.name


class Image(models.Model):
    image = models.ImageField('Дополнительные картинки', upload_to=additional_images_upload_root)
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='additional_images')
