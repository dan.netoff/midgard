from django.urls import path
from .views import *

app_name = 'orders'

urlpatterns = [
        path('show/<order_id>/', OrderDetailView.as_view(), name='order_detail'),
        path('create/', OrderCreateView.as_view(), name='order_create'),
        path('', OrderListView.as_view(), name='order_list'),
]
