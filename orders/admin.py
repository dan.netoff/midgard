from django.contrib import admin
from django.utils.translation import gettext_lazy as _
from .models import Order, OrderItem


class OrderPaymentTypeListFilter(admin.SimpleListFilter):

    title = _('Order payment_type')

    parameter_name = 'payment_type'

    def lookups(self, request, model_admin):
        return (('1', _('Наличные Курьеру')),
                ('2', _('Безналичные')),
                ('3', _('Бонусы'))
                )

    def queryset(self, request, queryset):
        # ****Tb час промучался... пока не обернул значения в стрингу!
        if self.value() == '1':
            return queryset.filter(payment_type=1)
        if self.value() == '2':
            return queryset.filter(payment_type=2)
        if self.value() == '3':
            return queryset.filter(payment_type=3)


class OrderStatusListFilter(admin.SimpleListFilter):

    title = _('Order Order by Status ;)')

    parameter_name = 'status'

    def lookups(self, request, model_admin):
        return (('1', _('Оплачен')),
                ('2', _('Неоплачен')),
                )

    def queryset(self, request, queryset):
        if self.value() == '1':
            return queryset.filter(status=1)
        if self.value() == '2':
            return queryset.filter(status=2)


class OrderItemInline(admin.TabularInline):
    model = OrderItem
    raw_id_fields = ['product']


class OrderAdmin(admin.ModelAdmin):
    list_display = ['id', 'customer', 'address',
                    'phone', 'status', 'payment_type', 'comment', 'total_quantity',
                    'created']
    list_filter = (OrderPaymentTypeListFilter, OrderStatusListFilter)
    inlines = [OrderItemInline]


admin.site.register(Order, OrderAdmin)
