from django.shortcuts import render, redirect, get_object_or_404

from product.models import Product
from .cart import Cart as Cart2
from .forms import CartAddProductForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views import View


class CartDetail(LoginRequiredMixin, View):
    login_url = 'login'
    def get(self, request):
        cart = Cart2(request)
        for item in cart:
            item['update_quantity_form'] = CartAddProductForm(
                initial={
                    'quantity': item['quantity'],
                    'update': True
                })
        return render(request, 'cart/detail.html', {'cart': cart})


class CartAdd(View):
    def get(self, request, product_id):
        cart = Cart2(request)
        product = get_object_or_404(Product, id=product_id)
        cart.add(product=product,
                 update_quantity=False)
        return redirect('cart:cart_detail')

    def post(self, request, product_id):
        cart = Cart2(request)
        product = get_object_or_404(Product, id=product_id)
        form = CartAddProductForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            cart.add(product=product,
                     quantity=cd['quantity'],
                     update_quantity=cd['update'])
        return redirect('cart:cart_detail')


class CartRemove(View):
    def get(self, request, product_id):
        cart = Cart2(request)
        product = get_object_or_404(Product, id=product_id)
        cart.remove(product)
        return redirect('cart:cart_detail')
