from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path, include
from django.views.generic import TemplateView, RedirectView

from accounts.views import SignupView, LogoutUserView, LoginUserView, ProfileView
#from cart.views import CartItemsListView, AddItemView, GetCartItemsTotalQuantity, EmptyCart


urlpatterns = [
                  path('admin/', admin.site.urls),
                  #path('cart', TemplateView.as_view(template_name='cart.html'), name='cart'),
                  #path('checkout', TemplateView.as_view(template_name='checkout.html'), name='checkout'),
                  #path('contact', TemplateView.as_view(template_name='contact.html'), name='contact'),
                  path('signup', SignupView.as_view(), name='signup'),
                  path('logout', LogoutUserView.as_view(), name='logout'),
                  path('login', LoginUserView.as_view(), name='login'),
                  #path('oauth', include('social_django.urls', namespace='social')),
                  path('', RedirectView.as_view(url='shop')),
                  path('shop/', include('product.urls', namespace='shop')),
                  path('profile/', ProfileView.as_view(), name='profile'),
                  #####path('cart/', CartItemsListView.as_view(), name='cart'),
                  path(r'cart/', include('cart.urls', namespace='cart')),
                  path(r'orders/', include('orders.urls', namespace='orders')),
                  #path('add-product/', AddItemView.as_view(), name='add_item'),
                  #path('cart-items-total-quantity/', GetCartItemsTotalQuantity.as_view(), name='cart-items-quantity'),
                  #path('empty-cart/', EmptyCart.as_view(), name='empty-cart'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

